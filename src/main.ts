import * as dotenv from 'dotenv';
dotenv.config();

import { config } from './config/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './common/services/logger.service';
import { LoggerInterceptor } from './common/interceptors/logger.interceptor';
import { InternalServerErrorException, ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';
import * as xss from 'xss-clean';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AllExceptionsFilter } from './common/filters/all-exceptions.filter';

async function bootstrap() {
  const logger: LoggerService = LoggerService.createLogger('Bootstrap');

  // Check peopoleapi
  if (!config.peopleapi.key) {
    throw new InternalServerErrorException(
      'People api key is missing. This application could not be started',
    );
  }

  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // Apply logger
  app.useGlobalInterceptors(new LoggerInterceptor());

  // Apply validation pipe
  app.useGlobalPipes(new ValidationPipe(config.validation));

  // Apply compression middleware
  app.use(compression());

  // Apply Protection middlewares
  app.use(helmet());
  app.use(helmet.hidePoweredBy());
  app.use(rateLimit({ max: 200, windowMs: 30 * 60 * 1000 }));
  app.use(xss());

  // Static files
  app.useStaticAssets(join(__dirname, '/../', config.upload.destinationFolder));

  // Apply exceptions filter
  app.useGlobalFilters(new AllExceptionsFilter());

  await app.listenAsync(config.port);
  logger.log(`Application is running on: ${await app.getUrl()}`);
}

try {
  bootstrap();
} catch (error) {
  console.error(error);
}
