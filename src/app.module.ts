import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { config } from './config/config';
import { AuthModule } from './core/auth/auth.module';
import { AuthController } from './core/auth/auth.controller';
import { CoreModule } from './core/core.module';
import { UploadModule } from './core/upload/upload.module';
import { SocketioModule } from './common/socketio/socketio.module';
import { MailModule } from './core/mail/mail.module';

@Module({
  imports: [
    TypegooseModule.forRoot(config.db.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    AuthModule,
    CoreModule,
    UploadModule,
    SocketioModule.register(),
  ],
  controllers: [AuthController],
})
export class AppModule {}
