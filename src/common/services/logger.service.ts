import {
  Injectable,
  LoggerService as NestLoggerService,
  Inject,
} from '@nestjs/common';
import { ErrorLog } from '../interfaces/error-log.interface';
import { MailService } from '../../core/mail/mail.service';

@Injectable()
export class LoggerService implements NestLoggerService {
  private mailService = new MailService();
  private readonly context: string;

  constructor(context?: string) {
    // this.mailService = new MailService();
    this.context = context;
  }

  static createLogger(context?: string): LoggerService {
    return new LoggerService(context);
  }

  public log(message: any, context?: string) {
    console.log(this.jsonFormat(message, context));
  }

  public error(error: Error, trace?: string, context?: string) {
    const errorLog: ErrorLog = {
      error: error.message,
      stack: error.stack,
      context: context ? context : '',
    };
    this.mailService.sendErrorMail(errorLog);
    console.log('\x1b[31m', this.jsonFormat(errorLog));
  }

  public warn(message: any, context?: string) {
    console.log(this.jsonFormat(message, context));
  }

  public debug?(message: any, context?: string) {
    console.log(this.jsonFormat(message, context));
  }

  public verbose?(message: any, context?: string) {
    console.log(this.jsonFormat(message, context));
  }

  private jsonFormat(message: any, context?: string) {
    if (!context) return JSON.stringify(message);
    return JSON.stringify({ message, context });
  }
}
