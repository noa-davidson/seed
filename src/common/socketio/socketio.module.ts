import { Module, DynamicModule } from '@nestjs/common';
import { SocketioGateway } from './socketio.gateway';
import { config } from 'src/config/config';
import { SocketioService } from './socket.io.service';
import { UsersService } from 'src/core/user/services/user.service';

@Module({})
export class SocketioModule {
  static register(): DynamicModule {
    return {
      module: SocketioModule,
      providers: config.socketIO
        ? [SocketioGateway, SocketioService, UsersService]
        : [],
    };
  }
}
