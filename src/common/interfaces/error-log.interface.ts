export interface ErrorLog {
  error: string;
  stack: string;
  context?: string;
}
