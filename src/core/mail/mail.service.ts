import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { config } from 'src/config/config';
import { UsersService } from '../user/services/user.service';
import { ErrorLog } from 'src/common/interfaces/error-log.interface';

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService?: MailerService,
    private usersService?: UsersService,
  ) {}

  async sendErrorMail(error: ErrorLog): Promise<void> {
    try {
      const recipients = await this.getRecipients();
      const subject = 'error occurred in meteor app';
      await this.sendMail(
        recipients,
        subject,
        error.error,
        this.generateHtml(error),
      );
    } catch (error) {
      console.log(error, 'sendErrorMail');
    }
  }

  async sendMail(
    receivers: string[],
    subject: string,
    text: string,
    html?: string,
  ): Promise<void> {
    try {
      await this.mailerService.sendMail({
        to: receivers,
        from: config.nodeMailer.config.auth.user,
        subject: subject,
        text: text,
      });
    } catch (error) {
      console.log(error, 'sendMail');
    }
  }

  private async getRecipients(): Promise<string[]> {
    try {
      const groups = config.nodeMailer.recipients.groups;
      const recipients: string[] = [];
      for (let i = 0; i < groups.length; i++) {
        const users = await this.usersService.getUsersInGroup(groups[i]);
        recipients.push(...users);
      }
      recipients.push(...config.nodeMailer.recipients.users);
      return recipients;
    } catch (error) {
      console.log(error);
    }
  }

  private generateHtml(error: ErrorLog): string {
    return `<h4>${error.context}</h4> <b>${error.error}</b> <p>${error.stack}</p>`;
  }
}
