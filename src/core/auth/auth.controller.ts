import { Controller, Post, UseGuards, Get } from '@nestjs/common';
import { AuthService } from './services/auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { GetUser } from 'src/core/user/decorators/get-user.decorator';
import { AccessToken } from './interfaces/access-token.interface';
import { LoggerService } from 'src/common/services/logger.service';

@Controller('auth')
export class AuthController {
  private logger = new LoggerService();
  constructor(private authService: AuthService) {}

  @Post('authenticate')
  async authenticate(@GetUser() user): Promise<AccessToken> {
    return this.authService.authenticate(user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('validateUser')
  validateUser(@GetUser() user): Promise<boolean> {
    return this.authService.validateUser(user);
  }
}
