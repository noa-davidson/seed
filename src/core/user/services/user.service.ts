import { Injectable } from '@nestjs/common';
import { UserDto } from '../dto/user.dto';
import { LoggerService } from 'src/common/services/logger.service';
import { JwtPayload } from 'src/core/auth/interfaces/jwt-payload.interface';

@Injectable()
export class UsersService {
  private logger = new LoggerService(UsersService.name);

  /**
   * Find user by username.
   * @param username
   */
  async findOne(username: string): Promise<UserDto | undefined> {
    const changeMe = new UserDto();
    changeMe.username = 'noa';
    changeMe.displayname = 'נועה המלכה';

    return changeMe;
  }

  async getUserDetails(user: JwtPayload): Promise<UserDto> {
    try {
      const userDetails: UserDto = await this.findOne(user.username);
      userDetails.roles = user.roles;
      return userDetails;
    } catch (error) {
      this.logger.error(error, 'getUserDetails');
    }
  }

  async isUserInGroup(username: string, group: string): Promise<boolean> {
    return true;
  }

  async getUsersInGroup(groupMame: string): Promise<string[]> {
    // TODO: change to peopole
    return ['noa'];
  }
}
