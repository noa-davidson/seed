import { Controller, UseGuards, Get } from '@nestjs/common';
import { JwtAuthGuard } from 'src/core/auth/guards/jwt-auth.guard';
import { UsersService } from '../services/user.service';
import { UserDto } from '../dto/user.dto';
import { GetUser } from '../decorators/get-user.decorator';
import { LoggerService } from 'src/common/services/logger.service';
import { JwtPayload } from 'src/core/auth/interfaces/jwt-payload.interface';

@UseGuards(JwtAuthGuard)
@Controller('api/user')
export class UserController {
  private logger = new LoggerService(UserController.name);
  constructor(private userService: UsersService) {}

  @Get()
  async userDetails(@GetUser() user: JwtPayload): Promise<UserDto> {
    return this.userService.getUserDetails(user);
  }
}
