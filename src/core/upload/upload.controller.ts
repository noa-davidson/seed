import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
  Get,
  Param,
  Res,
} from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from 'src/core/auth/decorators/roles.decorator';
import { LoggerService } from 'src/common/services/logger.service';
import { GetPathPipe } from './pipes/get-path.pipe';
import { PathValidationPipe } from './pipes/path-validation.pipe';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('/api/upload')
export class UploadController {
  private readonly logger = new LoggerService(UploadController.name);

  @Roles('admin')
  @Post()
  @UseInterceptors(AnyFilesInterceptor())
  uploadFile(@UploadedFiles() files) {
    this.logger.log(`uploaded files : ${files.toString()}`);
  }

  @Roles('admin', 'viewer')
  @Get('/:fileName')
  getFile(
    @Param('fileName', GetPathPipe, PathValidationPipe) file: string,
    @Res() res,
  ) {
    return res.sendFile(file);
  }
}
