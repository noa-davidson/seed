import { boolean, integer, stringArray } from './config.utils';
import { Config, Environment } from './config.interface';
import { HandlebarsAdapter } from '@nestjs-modules/mailer';

const defaultConfig: Config = {
  port: integer(process.env.PORT) || 8080,
  host: process.env.HOSt || 'localhost',
  socketIO: boolean(process.env.SOCKET_IO),
  validation: {
    transform: true,
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    validationError: { target: false },
  },
  environment: (process.env.NODE_ENV as Environment) || Environment.Local,
  peopleapi: {
    key: process.env.PEOPLEAPI_KEY || '',
  },
  db: {
    uri: process.env.DB_URI || 'mongodb://localhost:27017/test',
    type: process.env.DB_TYPE || 'mongo',
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '27017',
    username: process.env.DB_USERNAME || '',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || 'local',
  },
  auth: {
    secret: process.env.SECRET || 'ao&2k10~i!`',
    viewerUsers: stringArray(process.env.VIEWER_USERS) || [],
    viewerGroups: stringArray(process.env.VIEWER_GROUPS) || [],
    privilagedUsers: stringArray(process.env.PRIVILAGED_USERS) || [],
    privilagedGroups: stringArray(process.env.PRIVILAGED_GROUPS) || [],
    adminUsers: stringArray(process.env.ADMIN_USERS) || [],
    adminGroups: stringArray(process.env.ADMIN_GROUPS) || [],
    tokenExpiry: process.env.TOKEN_EXPIRY || '4h',
  },
  upload: {
    maxFiles: integer(process.env.MAX_FILES) || 5,
    maxSize: integer(process.env.MAX_SIZE) || 2000000000,
    types: stringArray(process.env.FILE_TYPES) || [],
    destinationFolder: process.env.UPLOAD_DEST || './uploads',
  },
  nodeMailer: {
    recipients: {
      users: stringArray(process.env.RECIPIENTS_USERS) || [],
      groups: stringArray(process.env.RECIPIENTS_GROUPS) || [],
    },
    config: {
      auth: {
        user: process.env.EMAIL_ID || '',
        pass: process.env.EMAIL_PASS || '',
      },
      tls: {
        rejectUnauthorized: boolean(process.env.REJECT_UNAUTHORIZED) || false,
      },
      host: process.env.EMAIL_HOST || 'smtp.gmail.com',
      port: integer(process.env.EMAIL_PORT) || 587,
      secure: boolean(process.env.EMAIL_SECURE) || false,
    },
  },
};

export default defaultConfig;
